$(document).ready(function () {
    let staticHeight = $('.white-logo').offset().top;

    const changeHeaderScroll = () => {
        if ($(window).scrollTop() > staticHeight) {
            $('.header').css({
                'backgroundColor': "black",
                'height': "60px"
            });
            $('#upBtn').fadeIn('slow');
        } else {
            $('.header').css({
                'backgroundColor': "rgba(0,0,0,0.5)",
                'height': "100px"
            });
            $('#upBtn').fadeOut('slow');
        }
    }

    $(window).scroll(changeHeaderScroll);

    $('#upBtn').click(() => {
        $(document.documentElement).animate({
            scrollTop: $(document.body).offset().top
        }, 1000);
    })

    $('.fog-lake>p:first').click(() => {
        $('.fog-lake>p:last').slideToggle('slow');
    });

    $('.nav-bar>li').click((e) => {
        let clicked = $(e.target).index();
        switch (clicked) {
            case 1:
                $(document.documentElement).animate({
                    scrollTop: $('.red-speed').offset().top - 60
                }, 1000);
                break;
            case 2:
                $(document.documentElement).animate({
                    scrollTop: $('.fog-tree').offset().top - 60
                }, 1000);
                break;
            case 3:
                $(document.documentElement).animate({
                    scrollTop: $('.fog-lake').offset().top - 60
                }, 1000);
                break;
            default:
                $(document.documentElement).animate({
                    scrollTop: $(document.body).offset().top
                }, 1000);
        }
    });

});